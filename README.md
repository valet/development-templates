# VALET Development Templates

This project contains a set of (simple) project templates that integrate VALET into a build process utilitizing the Unix `make` utility.

## Makefiles

The [Makefiles](./Makefiles) directory contains the make files that effect the environment setup and build of the software.  The `Makefile` in each individual project sets variables and invokes make on these files.

## Projects

Inside the [Projects](./Projects) directory are each kind of (simple) project:

- Executable from C source code
- Executable from Fortran source code
- Executable from mixed C and Fortran source code
- Executable from MATLAB code

Users can copy the entire directory (using the name of the project) and edit the `Makefile` according to the instructions therein.
