/*
// helloworld
// printmsg.c
//
// Functions that print helloworld messages.
//
// $Id: printmsg.c 403 2012-07-16 14:19:56Z frey $
//
*/

/* Include our own header: */
#include "printmsg.h"

/* Include any other headers: */
#include <stdio.h>

void
printmsg(void)
{
  printf("Hello, world!\n\n");
}

/**/

void
printmsg1(
  const char      *arg1
)
{
  printf("Hello, world!  Did you know: %s\n\n", arg1);
}
