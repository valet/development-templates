/*
// helloworld
// printmsg.h
//
// Public function protypes for the functions defined in printmsg.c.
//
// $Id: printmsg.h 403 2012-07-16 14:19:56Z frey $
//
*/

#ifndef __PRINTMSG_H__
#define __PRINTMSG_H__

/*!
  @function printmsg
  @discussion
    Prints the standard "hello world" message to stdout.
*/
void printmsg(void);

/*!
  @function printmsg1
  @discussion
    Prints the standard "hello world" message and a trailing
    personalized portion (arg1) to stdout.
*/
void printmsg1(const char *arg1);

#endif /* __PRINTMSG_H__ */

