/*
// helloworld
// helloworld.c
//
// The main() function is the program's entry point.
//
// $Id: helloworld.c 403 2012-07-16 14:19:56Z frey $
//
*/

/* Include the interfaces declared in printmsg.h since we'll be using
   its functions: */ 
#include "printmsg.h"


int
main(
  int       argc,
  char*     argv[]
)
{
  printmsg();
  printmsg1("the earth's diameter is 7,926.41 miles at the equator?");
  
  return 0;
}

