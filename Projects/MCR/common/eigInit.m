function rndMat = eigInit(dim, lambda)
rndMat = rand(dim) + lambda * diag(rand(dim,1));
end
