function maxe = maxEig(lam)
if (isdeployed)
    lam=str2num(lam);
end

M=eigInit(1000, lam);

maxe = max(eig(M))
end
