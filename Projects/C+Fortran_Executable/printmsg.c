/*
// helloworld
// printmsg.c
//
// Functions that print helloworld messages.
//
// $Id: printmsg.c 405 2012-07-16 15:21:51Z frey $
//
*/

/* Include any other headers: */
#include <stdio.h>

void
__fputs_fstring(
  FILE*           fptr,
  const char      *s,
  int             slen
)
{
  while ( slen-- ) fputc(*s++, fptr);
}

/**/

void
printmsg_(void)
{
  printf("Hello, world!\n\n");
}

/**/

void
printmsg1_(
  const char      *arg1,
  int             arg1_length
)
{
  printf("Hello, world!  Did you know: ");
  __fputs_fstring(stdout, arg1, arg1_length);
  printf("\n\n");
}
