C
C helloworld
C printmsg.f
C
C Functions that print helloworld messages.
C
C $Id: printmsg.f 403 2012-07-16 14:19:56Z frey $
C
      Subroutine PrintMsg()

      Print *,'Hello, world!'
      Return
      End

      Subroutine PrintMsg1(arg1)

      Character (len=*) :: arg1
      Print *,'Hello, world! Did you know ' // arg1
      Return
      End

